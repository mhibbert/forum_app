from django.shortcuts import render
from threads.models import Subject

def home(request):
    return render(request, 'index.html', { 'subjects': Subject  .objects.all() })