from django.conf.urls import include, url
from django.contrib import admin

from rest_framework import routers
from .views import threads, thread, edit_thread, new_thread, new_post, edit_post, delete_post, thread_vote
from rest_views import PollViewSet, PollInstanceView, VoteInstanceView, PostUpdateView, PostDeleteView

urlpatterns = [
    url(r'^threads/(?P<subject_id>\d+)/$', threads, name='threads'),
    url(r'^thread/(?P<thread_id>\d+)/$', thread, name='thread'),
    url(r'^thread/new/(?P<subject_id>\d+)/$', new_thread, name='new_thread'),
    url(r'^thread/edit/(?P<thread_id>\d+)/$', edit_thread, name='edit_thread'),
    url(r'^thread/vote/(?P<thread_id>\d+)/(?P<subject_id>\d+)/$', thread_vote, name='cast_vote'),
    url(r'^post/new/(?P<thread_id>\d+)/$', new_post, name='new_post'),
    url(r'^post/edit/(?P<thread_id>\d+)/(?P<post_id>\d+)/$', edit_post, name='edit_post'),
    url(r'^post/delete_post/(?P<post_id>\d+)/$', delete_post, name='delete_post'),
    url(r'^polls/$', PollViewSet.as_view()),
    url(r'^polls/(?P<pk>[\d]+)/$', PollInstanceView.as_view(), name='poll-instance'),
    url(r'^polls/vote/(?P<thread_id>[\d]+)/$', VoteInstanceView.as_view(), name='vote-instance'),
    url(r'^post/update/(?P<pk>[\d]+)/$', PostUpdateView.as_view(), name='update-poll'),
    url(r'^post/delete/(?P<pk>[\d]+)/$', PostDeleteView.as_view(), name='delete-poll'),
]