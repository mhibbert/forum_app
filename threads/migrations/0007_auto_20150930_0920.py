# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('threads', '0006_auto_20150929_1431'),
    ]

    operations = [
        migrations.CreateModel(
            name='Vote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('poll', models.OneToOneField(to='threads.Poll')),
            ],
        ),
        migrations.RemoveField(
            model_name='pollsubject',
            name='votes',
        ),
        migrations.AlterField(
            model_name='posts',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 30, 9, 20, 37, 342790, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='thread',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 30, 9, 20, 37, 341859, tzinfo=utc)),
        ),
        migrations.AddField(
            model_name='vote',
            name='subject',
            field=models.ForeignKey(related_name='votes', to='threads.PollSubject'),
        ),
        migrations.AddField(
            model_name='vote',
            name='user',
            field=models.ForeignKey(related_name='votes', to=settings.AUTH_USER_MODEL),
        ),
    ]
