# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc
import tinymce.models
from django.conf import settings
from django.utils import timezone


class Migration(migrations.Migration):

    dependencies = [
        ('threads', '0002_auto_20150821_1037'),
    ]

    operations = [
        migrations.AddField(
            model_name='thread',
            name='created_at',
            field=models.DateTimeField(default=timezone.now()),
        ),
        migrations.AlterField(
            model_name='posts',
            name='created_at',
            field=models.DateTimeField(default=timezone.now()),
        ),
        migrations.AlterField(
            model_name='posts',
            name='comment',
            field=tinymce.models.HTMLField(blank=True),
        ),
        migrations.AlterField(
            model_name='posts',
            name='user',
            field=models.ForeignKey(related_name='posts', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='thread',
            name='user',
            field=models.ForeignKey(related_name='threads', to=settings.AUTH_USER_MODEL),
        ),
    ]
