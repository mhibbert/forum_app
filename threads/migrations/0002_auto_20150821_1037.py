# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import tinymce.models
from django.utils import timezone
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('threads', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='posts',
            name='created_at',
            field=models.DateTimeField(default=timezone.now()),
        ),
        migrations.AlterField(
            model_name='subject',
            name='description',
            field=tinymce.models.HTMLField(),
        ),
    ]
