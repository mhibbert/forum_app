# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('threads', '0008_auto_20150930_0933'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='vote',
            name='ip_address',
        ),
        migrations.AlterField(
            model_name='posts',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 30, 10, 14, 20, 928844, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='thread',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 30, 10, 14, 20, 927917, tzinfo=utc)),
        ),
    ]
