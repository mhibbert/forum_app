from rest_framework import status
from rest_framework.response import Response
from rest_framework import generics
from models import Poll, Vote, Thread, PollSubject, Posts
from serializers import PollSerializer, VoteSerializer, PostSerializer


class PollViewSet(generics.ListAPIView):
    queryset = Poll.objects.all()
    serializer_class = PollSerializer
        
class PollInstanceView(generics.RetrieveAPIView):
    queryset = Poll.objects.all()
    serializer_class = PollSerializer
    
class VoteInstanceView(generics.ListCreateAPIView):
    serializer_class = VoteSerializer
    queryset = Vote.objects.all()
    
    def create(self, request, thread_id):
        thread = Thread.objects.get(id=thread_id)
        subject = thread.poll.votes.filter(user=request.user).first()
        
        if subject:
            return Response({"error": "already voted!"}, status=status.HTTP_400_BAD_REQUEST)  
          
        request.data['user'] = request.user.id
        serializer = VoteSerializer(data=request.data)
    
        if serializer.is_valid():
            self.perform_create(serializer)
            thread.poll.votes.add(serializer.instance)
            
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED,
                            headers=headers)
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)        
    
class PostUpdateView(generics.UpdateAPIView):
    serializer_class = PostSerializer
    queryset = Posts.objects.all()    
    
    
class PostDeleteView(generics.DestroyAPIView):
    serializer_class = PostSerializer
    queryset = Posts.objects.all()    
    
